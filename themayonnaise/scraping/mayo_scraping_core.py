import datetime as dt
import numpy as np
import pandas as pd

import glob
import os
import requests
import time


from bs4 import BeautifulSoup
from io import BytesIO
from stem import Signal
from stem.control import Controller
from zipfile import ZipFile

from themayonnaise import config
from themayonnaise.scraping.mayo_user_agents import generate_user_agent_headers


def keep_quarters_within_bounds(quarter:int, year:int):
    while quarter < 1:
        year = year -1
        quarter = quarter + 4
    while quarter > 4:
        year = year + 1
        quarter = quarter - 4
    return quarter, year
            
def extract_year_and_quarter_ints_from_datestring(date_string:str):
    quarters = {'03-31':1, '06-30':2, '09-30':3, '12-31':4}
    year = int(date_string[:4])
    quarter = quarters[date_string[5:]]
    return year, quarter

def compose_datestring_from_year_and_quarter_ints(quarter:int, year:int):
    quarters = {1:'03-31', 2:'06-30', 3:'09-30', 4:'12-31'}
    return f'{year:04d}-{quarters.get(quarter)}'

def quarter_arithmatic_on_date_string(date_string:str, quarters:int):
    current_year, current_quarter = extract_year_and_quarter_ints_from_datestring(date_string)
    current_quarter += quarters
    current_year, current_quarter = keep_quarters_within_bounds(current_quarter, current_year)
    return compose_datestring_from_year_and_quarter_ints(current_year, current_quarter)

def batch_clean_scraped_tabular_data(df:pd.core.frame.DataFrame, numeric_columns:list, string_columns:list, date_columns:list,
                                     date_format:str=None):
    for column in numeric_columns:
        if not df[column].isna().all():
            df[column] = pd.to_numeric(df[column].str.replace(',', ''))
    for column in string_columns:
        if not df[column].isna().all():
            df[column] = df[column].str.strip()
    for column in date_columns:
        df[column] = pd.to_datetime(df[column], format=date_format)
    return df

def extract_basic_soup_table_to_nested_list(soup, css_selector:str, column_overrides:dict={}):
    rows = soup.select(css_selector + ' tr')
    if len(column_overrides) == 0:
        columns = [column.text for column in rows[0].select('th')]
    else:
        columns = [column_overrides.get(i, column.text) for i, column in enumerate(rows[0].select('th'))]
    entries = [[entry.text for entry in row.select('td')] for row in rows[1:]]
    return columns, entries

def extract_basic_soup_table_to_dataframe(soup, css_selector:str, manual_columns=[], column_overrides:dict={}):
    columns, entries = extract_basic_soup_table_to_nested_list(soup, css_selector, column_overrides)
    df =  pd.DataFrame(entries, columns=columns).replace(regex='\s+', value=' ').replace(regex='^ $', value='').replace('', np.nan)
    return df[manual_columns] if len(manual_columns) > 0 else df.dropna(axis=1, how='all')

def weighted_centered_random_variable(base:float, weight:float):
    return base * (1-weight) + 2 * base * np.random.random() * weight

def random_time_sleep(base_seconds:float, random_weight:float=.5, ordered_chaos:bool=False):
    sleep = weighted_centered_random_variable(base_seconds, random_weight)
    if ordered_chaos:
        sleep = sleep * np.log(sleep) * np.log(sleep) * 1/np.cbrt(weighted_centered_random_variable(base_seconds, random_weight))
    time.sleep(sleep)
    
def get_requests_scraping_session(headers:dict={}, port:int=None, tor:bool=False):
    c = config.Config()
    if not port and tor:
        port = c.Tor_session_port
    session = requests.session()
    if tor:
        # Tor uses the 9050 port as the default socks port
        session.proxies = {'http':  f'socks5://localhost:{port}',
                           'https': f'socks5://localhost:{port}'}
    session.headers.update(headers)
    return session

def renew_tor_connection(controller_port:int=None, password:str=None):
    c = config.Config()
    if not controller_port:
        controller_port = c.Tor_controller_port
    if not password:
        password = c.Tor_controller_password
    with Controller.from_port(port = controller_port) as controller:
        controller.authenticate(password=password)
        controller.signal(Signal.NEWNYM)
        controller.close()

def get_url_content_from_session(session, url:str):
    page = session.get(url)
    if page.status_code != 200:
        raise RuntimeError(f'Error request recieved status code of {page.status_code}')
    return page.content

def get_url_content_from_session_override_codes(session, url:str, allow_codes:list):
    page = session.get(url)
    if page.status_code == 200:
        return page.content, page.status_code
    elif page.status_code in allow_codes:
        print(f'Empty results for {url} due to status code of {page.status_code}')
        return None, page.status_code
    else:
        raise RuntimeError(f'Error request recieved status code of {page.status_code}')

def get_url_content_to_soup(session, url:str, parser:str='html.parser'):
    content = get_url_content_from_session(session, url)
    return BeautifulSoup(content, parser)

def get_url_content_to_soup_override_codes(session, url:str, allow_codes:list, parser:str='html.parser'):
    content, code = get_url_content_from_session_override_codes(session, url, allow_codes)
    if content is None:
        return None, code
    else:
        return BeautifulSoup(content, parser), code

def check_empty_soup_after_css(soup, css_selector:str):
    return len(soup.select(css_selector)) == 0


def download_from_zip(session, url:str, output_dir:str, archive_rename='', extract=True):
    os.makedirs(output_dir, exist_ok=True)
    r = get_url_content_from_session(session, url)
    
    if not extract:
        if archive_rename:
            filename = archive_rename
        else:
            filename = os.path.split(url)[1]
        return save_file_general(r, os.path.join(output_dir, filename), binary=True)
    z = ZipFile(BytesIO(r))
    return z.extractall(output_dir)
    
    
def save_file_general(file_data, path:str, binary:bool=False, append:bool=False):
    mode = 'a' if append else 'w'
    mode = mode + 'b' if binary else mode
    with open(path, mode) as f:
        f.write(file_data)
        
def open_file_general(path:str, binary:bool=False, open_kwargs:dict={}):
    mode = 'rb' if binary else 'r'
    with open(path,'r', **open_kwargs) as f:
        return f.read()

def open_file_lines_general(path:str, open_kwargs:dict={}):
    with open(path, 'r', **open_kwargs) as f:
        return f.readlines()

def save_file_lines_general(lines:list, path:str):
    with open(path, "w") as f:
        f.writelines(lines)

def remove_empty_csvs(path:str):
    for csv in glob.glob(path + '/*.csv'):
        if pd.read_csv(csv).empty:
            os.remove(csv)
        
def compiled_regex_batch_replace(s:str, compiled_regex_patches:list):
    for regex, replacement in compiled_regex_patches:
        s = regex.sub(replacement, s)
    return s

def examine_csv_delimiter(path:str, delimiter:str, max_per_line:int, compiled_regex_patches:list, open_kwargs:dict={}):
    
    lines = open_file_lines_general(path, open_kwargs)
    for line in lines:
        if line.count(delimiter) > max_per_line:
            print(line)
            line = compiled_regex_batch_replace(line, compiled_regex_patches)
            print(line)

def repair_csv_delimiter(path:str, delimiter:str, max_per_line:int, compiled_regex_patches:list, open_kwargs:dict={}):
    
    lines = open_file_lines_general(path, open_kwargs)
    for i in range(len(lines)):
        line = lines[i]
        if line.count(delimiter) > max_per_line:
            lines[i] = compiled_regex_batch_replace(line, compiled_regex_patches)
    save_file_lines_general(lines, path)
    
def get_column_and_date_list_from_period_df(df, expand:int=1, column:str='CIK', as_datetime_objects:bool=True):
    if as_datetime_objects:
        f = '%Y-%m-%d'
        return [[row[1][column], (dt.datetime.strptime(quarter_arithmatic_on_date_string(row[1]['Datemin'], -expand), f), dt.datetime.strptime(quarter_arithmatic_on_date_string(row[1]['Datemax'], expand),f))] for row in df.iterrows()]
    else:
        return [[row[1][column], (quarter_arithmatic_on_date_string(row[1]['Datemin'], -expand), quarter_arithmatic_on_date_string(row[1]['Datemax'], expand))] for row in df.iterrows()]
    
    
def resample_to_quarter_ends(df, date_column, count_column):
    '''Takes a column of dates and values and resamples them inrerpolating observations to each quarter'''
    df = df.set_index(date_column).resample('1D').interpolate(method='time').reset_index()
    df = df[df[date_column].dt.is_quarter_end].dropna(subset=[count_column])
    df[count_column] = df[count_column].astype(int)
    return df
        
class General_Scraper(object):
    
    def __init__(self, save_directory:str='output'):
        
        self.save_directory = save_directory
        self.session = None
    
    def _aggregate_csvs(self, path:str, reverse:bool=False, extension:str='csv', csv_read_kwargs:dict={}):
        csvs = self._get_csv_list(path, extension)
        if reverse:
            csvs.reverse()
        return pd.concat([pd.read_csv(csv, **csv_read_kwargs) for csv in csvs], axis=0, ignore_index=True)
    
    def _consolidate_csvs(self, input_csv_directory:str, output_csv_path:str, reverse:bool=False, csv_read_kwargs:dict={}):
        df = self._aggregate_csvs(input_csv_directory, reverse, csv_read_kwargs=csv_read_kwargs)
        df.to_csv(output_csv_path, index=False)
        
    def _download_raw_From_url_to_disk(self, url:str, dir_path:str):
        fn = os.path.join(dir_path, os.path.basename(url))
        save_file_general(self._get_from_url(url, soup_parser=None), fn, binary=True)
    
    def _get_csv_list(self, path:str, extension:str='csv'):
        return sorted(glob.glob(path + f'/**/*.{extension}', recursive=True))
    
    def _get_csv_list_filenames(self, path:str, extension:str='csv', trim_extension:bool=False):
        if trim_extension:
            return [os.path.splitext(os.path.basename(file))[0] for file in self._get_csv_list(path, extension)]
        else:
            return [os.path.basename(file) for file in self._get_csv_list(path, extension)]

    
    def _get_from_url(self, url:str, soup_parser:str='html.parser'):
        if soup_parser:
            return get_url_content_to_soup(self.session, url, soup_parser)
        else:
            return get_url_content_from_session(self.session, url)
        
    def _get_from_url_override_codes(self, url:str, allow_codes:list, soup_parser:str='html.parser'):
        if soup_parser:
            return get_url_content_to_soup_override_codes(self.session, url, allow_codes, soup_parser)
        else:
            return get_url_content_from_session_override_codes(self.session, url, allow_codes)
    
    def _initialize_output_dirs(self, save_directory:str, subdirectories:list=[]):
        if len(subdirectories) == 0:
            os.makedirs(save_directory, exist_ok=True)
        else:
            [os.makedirs(os.path.join(save_directory, directory), exist_ok=True) for directory in subdirectories]
        
        
class Time_Random_Sleeped_Scraper(General_Scraper):
    def __init__(self, time_sleep_base:float, time_sleep_random_weight:float=.5, save_directory:str='output'):
        
        super().__init__(save_directory)
        self.time_sleep_base = time_sleep_base
        self.time_sleep_random_weight = time_sleep_random_weight

    def _sleep(self, hibernate:bool=False):
        random_time_sleep(self.time_sleep_base, self.time_sleep_random_weight, hibernate)
        
        
class Time_Constant_Sleeped_Scraper(General_Scraper):
    def __init__(self, time_sleep:float, save_directory:str='output'):
        
        super().__init__(save_directory)
        self.time_sleep = time_sleep

    def _sleep(self):
        time.sleep(self.time_sleep)
        
        
class Tor_IP_Rotating_Scraper(Time_Random_Sleeped_Scraper):
    
    def __init__(self, time_sleep_base:float, time_sleep_random_weight:float=.5, save_directory:str='output',
                 batch_size:int=10, batch_size_random_weight:float=.5, print_ips:bool=False):
        super().__init__(time_sleep_base, time_sleep_random_weight, save_directory)
        self.batch_size = batch_size
        self.batch_size_random_weight = batch_size_random_weight
        self.print_ips = print_ips
        self.session_scrape_count = 0
        self.reset_threshold = 0
        self.update_requests_session()
        
    def _get_from_url(self, url:str, soup_parser:str='html.parser'):
        self.session_scrape_count += 1
        return get_url_content_to_soup(self.session, url, soup_parser)
        
    def _initialize_reset_threshold(self):
        self.reset_threshold = round(weighted_centered_random_variable(self.batch_size, self.batch_size_random_weight))
        
    def update_requests_session(self):
        if self.session and self.print_ips:
            print(self.session.get('https://api.ipify.org').text)
        headers = generate_user_agent_headers()
        renew_tor_connection()
        self.session = get_requests_scraping_session(headers=headers, tor=True)
        if self.print_ips:
            print(self.session.get('https://api.ipify.org').text)
        self.session_scrape_count = 0
        self._initialize_reset_threshold()
        
        
class SEC_General_Scraper(Time_Constant_Sleeped_Scraper):
    
    def __init__(self, time_sleep:float=.3, save_directory:str='output'):
        super().__init__(time_sleep, save_directory)
        self._initialize_requests_session()
        
    def _initialize_requests_session(self, user_agent:str=''):
        c = config.Config
        if not user_agent:
            user_agent = c.Edgar_user_agent
        headers = {'User-Agent':user_agent, 'Accept-Encoding': 'gzip, deflate', 'Host':'www.sec.gov'}
        self.session = get_requests_scraping_session(headers=headers)
import pandas as pd

import re

from themayonnaise.scraping.mayo_scraping_core import *

FTD_CSV_REPAIR_STRINGS = [[r'059361105\|BFUN\|(\d+)\|BAM\| ENTMT INC', r'059361105|BFUN|\1|BAM ENTMT INC'],
                          [r'105666101\|BRVO\|(\d+)\|BRAVO\| FOODS INTERNATIONAL CP', r'105666101|BRVO|\1|BRAVO FOODS INTERNATIONAL CP'],
                          [r'23344K110\|DMYQWS\|(\d+)\|DMY TECHNOLOGY GROUP INC IV \| ', r'23344K110|DMYQWS|\1|DMY TECHNOLOGY GROUP INC IV'],
                          [r'302311105\|EZEN\|(\d+)\|EZENIA\| INC', r'302311105|EZEN|\1|EZENIA INC'],
                          [r'56086P202\|MMUS\|(\d+)\|MAKEMUSIC\| INC NEW', r'56086P202|MMUS|\1|MAKEMUSIC INC NEW'],
                          [r'738754100\|POWN\|(\d+)\|POW\| ENTERTAINMENT INC', r'738754100|POWN|\1|POW ENTERTAINMENT INC'],
                          [r'988498101\|YUM\|(\d+)\|YUM\| BRANDS, INC', r'988498101|YUM|\1|YUM BRANDS, INC'],]
FTD_CSV_REPAIR_STRINGS = [[re.compile(pattern), replacement] for pattern, replacement in FTD_CSV_REPAIR_STRINGS]


class FTD_Scraper(SEC_General_Scraper):
    
    def __init__(self, time_sleep:float=.3, save_directory:str='ftds'):
        super().__init__(time_sleep, save_directory)
        self.base_url = 'https://www.sec.gov'
        self.fail_url = self.base_url + '/data/foiadocsfailsdatahtm'
        
        self.regex_patches = FTD_CSV_REPAIR_STRINGS
        self._initialize_output_dirs(save_directory)
    
    def _exclude_downloaded_urls(self, urls:list):
        orig_length = len(urls)
        downloaded = [os.path.splitext(file)[0] for file in os.listdir(self.save_directory)]
        urls = [url for url in urls if not self._check_url_isdownloaded(url, downloaded)]
        print(f'Already downloaded {orig_length - len(urls)} of {orig_length}')
        return urls
    
    def _check_url_isdownloaded(self, url:str, downloaded:list):
        filename = os.path.splitext(os.path.split(url)[1])[0]
        return filename in downloaded
    
    def _get_ftd_urls(self):
        soup = self._get_from_url(self.fail_url)
        css_selector = '.associated-data-distribution .list td a'
        return [self.base_url + link['href'] for link in soup.select(css_selector, href=True)]
    
    def scrape(self):
        urls = self._get_ftd_urls()
        urls = self._exclude_downloaded_urls(urls)
        for url in urls:
            download_from_zip(self.session, url, self.save_directory, extract=True)
            self._sleep()
            
    def repair_corrupted_csvs(self):
        print('Fixing unparseable csvs')
        for path in self._get_csv_list(self.save_directory, extension='txt'):
            try:
                repair_csv_delimiter(path, delimiter='|', max_per_line=5, compiled_regex_patches=self.regex_patches)
            except Exception as e:
                repair_csv_delimiter(path, delimiter='|', max_per_line=5, compiled_regex_patches=self.regex_patches,
                                     open_kwargs={'encoding':'windows-1252'})
                
    def fail_csvs_to_df(self):
        df = self._aggregate_csvs(self.save_directory, reverse=False, extension='txt', csv_read_kwargs={'sep':'|', 'dtype':{'CUSIP':str}})
        df = df.dropna(subset=['PRICE'])
        df['PRICE'] = df['PRICE'].replace('.', np.nan)
        df['SYMBOL'] = df['SYMBOL'].replace(r'^\*+$', np.nan, regex=True)
        df['SETTLEMENT DATE'] = pd.to_datetime(df['SETTLEMENT DATE'], format='%Y%m%d')
        df = df.astype({'QUANTITY (FAILS)':'int64', 'PRICE':float})
        df = df.sort_values(by=['SETTLEMENT DATE', 'SYMBOL']).reset_index(drop=True)
        return df
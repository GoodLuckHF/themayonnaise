import datetime as dt
import pandas as pd

import glob
import locale
import os
import re

import edgar

from sklearn.neighbors import LocalOutlierFactor
from sklearn.preprocessing import StandardScaler
from w3lib.html import replace_entities, remove_tags
from arelle import Cntlr, ModelManager, FileSource, PluginManager
#from arelle.plugin import saveLoadableOIM
#from arelle import PluginManager

from crayons.regex_crayons import *
from themayonnaise.scraping.mayo_scraping_core import *
from secxxxy_edgar_vectors.edgar_regex_patterns import *

    
class Edgar_Scraper(SEC_General_Scraper):
    
    def __init__(self, time_sleep:float=.3, save_directory:str='edgar',
                 edgar_index_from_year:int=1993):
        super().__init__(time_sleep, save_directory)
        self.filing_index_dir = 'sec_filings_index'
        self.temp_dir = 'temp'
        self.xrbl_dir = 'xrbl_data'
        self.text_dir = 'text_data'
        self.output_dir = 'output'
        self.skip_file = 'skip.txt'
        self.symbol_cik_mappings_file = 'ticker.txt'
        self.edgar_index = None
        self.cik_symbols = None
        self.base_url = 'https://www.sec.gov/Archives/'
        
        self._initialize_output_dirs(save_directory, subdirectories=[self.filing_index_dir, self.temp_dir, self.xrbl_dir, self.text_dir, self.output_dir])
        self._initialize_edgar_index(edgar_index_from_year)
        self._initialize_skip_file()

    
    def _aggregate_edgar_tsvs(self):
        print('Aggregating downloaded tsv files. This may take a moment')
        directory = os.path.join(self.save_directory, self.filing_index_dir)
        csv_read_kwargs = {'sep':'|','lineterminator':'\n', 'parse_dates':['date'],
                           'names':['cik', 'company', 'form', 'date' ,'text_url', 'html_url']}

        self.edgar_index = self._aggregate_csvs(directory, extension='tsv', csv_read_kwargs=csv_read_kwargs)
        
    def _clear_temp(self):
        path = os.path.join(self.save_directory, self.temp_dir)
        for file in os.listdir(path):
            os.remove(os.path.join(path, file))
        
    def _download_urls_to_temp(self, urls:list):
        for url in urls:
            self._download_raw_From_url_to_disk(url, os.path.join(self.save_directory, self.temp_dir))
            self._sleep()
            
    def _initialize_symbol_mappings(self):
        
        csv_path = os.path.join(self.save_directory, self.symbol_cik_mappings_file)
        if os.path.exists(csv_path):
            print('Importing symbol to cik mappings')
            self._import_symbol_mappings('https://www.sec.gov/include/ticker.txt')
        else:
            print('Downloading symbol to cik mappings')
            self._scrape_symbol_mappings(csv_path)
    
    
    def _initialize_edgar_index(self, year:int=1993):
        path = os.path.join(self.save_directory, self.filing_index_dir)
        if len(self._get_csv_list(path, extension='tsv')) == 0:
            self.edgar_index_download(year)
        else:
            self._aggregate_edgar_tsvs()
            
    def _initialize_skip_file(self):
        path = os.path.join(self.save_directory, self.skip_file)
        if not os.path.exists(path):
            save_file_general('', path)
            
    def _join_partial_url(self, url_end:str):
        return os.path.join(self.base_url, url_end)
        
    def edgar_index_download(self, year:int=1993):
        print('Scraping index tsvs')
        user_agent = self.session.headers['User-Agent']
        edgar.download_index(os.path.join(self.save_directory, self.filing_index_dir), year, user_agent, skip_all_present_except_last=False)
        self._aggregate_edgar_tsvs()
        
        
class Edgar_XRBL_Scraper(Edgar_Scraper):
    
    def __init__(self, time_sleep:float=.1, save_directory:str='edgar',
                 edgar_index_from_year:int=1993):
        super().__init__(time_sleep, save_directory, edgar_index_from_year)
        self._html_tag_styling_regex = CLEAN_TAGS_REGEX
    
    def _load_xrbl(self, path:str):
        ctlr = Cntlr.Cntlr()
        modelManager = ModelManager.initialize(ctlr)
        filesource = FileSource.FileSource(path)
        return modelManager.load(filesource)
    
    def _xrbl_to_df(self, xrbl, strip_styling:bool=False):
        value_col = 'value'
        # Class of shares is hidden among dimensions attribute of XRBL data. The length of dimensions is variable and may require
        # modifications to capture data for other usecases. For now dim00 and dim01 capture the bare minumum.
        columns = ['fact', value_col, 'dim00', 'dim01', 'isNumeric', 'contextID', 'isStartEndPeriod', 'isInstantPeriod',
                   'isForeverPeriod', 'startDatetime', 'endDatetime', 'unitID']
        facts = [(fact.concept.qname,
                  fact.text,
                  self._extract_from_xrbl_dimension(fact, 0, 0),
                  self._extract_from_xrbl_dimension(fact, 0, 1),
                  fact.isNumeric,
                  fact.contextID,
                  fact.context.isStartEndPeriod,
                  fact.context.isInstantPeriod,
                  fact.context.isForeverPeriod,
                  fact.context.startDatetime,
                  fact.context.endDatetime,
                  fact.unitID) if fact.context is not None else (fact.concept.qname,
                  fact.text,
                  fact.isNumeric,
                  fact.contextID,
                  None,
                  None,
                  None,
                  None,
                  None,
                  fact.unitID) for fact in xrbl.facts]
        if strip_styling:
            return self._remove_html_styling_from_xrbl_fact_df(pd.DataFrame(facts, columns=columns), value_col)
        else:
            return pd.DataFrame(facts, columns=columns)
    
    def _datatable_soup_to_df(self, soup):
        url_func = lambda x: os.path.join(os.path.dirname(self.base_url[:-1]), x.find('a')['href'][1:])
        columns=[column.text for column in soup.find_all('th')[1:4]]
        df = pd.DataFrame([row.find_all('td')[1:4] for row in soup.select('tr')[1:]], columns=columns)
        for column in ['Description', 'Type']:
            df[column] = df[column].map(lambda x: x.text)
        df['Document'] = df['Document'].map(url_func)
        return df
    
    def _download_data_table(self, url:str):
        soup = self._get_from_url(url)
        return self._select_data_table(soup)
    
    def _select_data_table(self, soup):
        return soup.select('#formDiv table[summary="Data Files"]')
    
    def _parse_xrbl_number_manual(self, number_String:str):
        '''This function's use of the XRBL is unstable for parsing numbers which may be due to the plugin
        text2num.py being skipped, or something else. For now numbers are handled manually with the locale module.'''

        loc = locale.getlocale()
        try:
            locale.setlocale( locale.LC_ALL, 'en_US.UTF-8')
            number = locale.atof(number_String)
            locale.setlocale( locale.LC_ALL, loc)
        except Exception as e:
            locale.setlocale( locale.LC_ALL, loc)
            raise e
        return number
    
    def _remove_html_styling_from_xrbl_fact_df(self, df, column:str):

        for pattern, replacement in self._html_tag_styling_regex:
            df[column] = df[column].str.replace(pattern, replacement, regex=True)
        return df
    
    def _extract_xrbl_facts_from_datatable(self, soup, save_raw:bool=False, print_url:str='', strip_styling:bool=True, raw_xrbl:bool=False):
        df = self._datatable_soup_to_df(soup)
        extracted_condition = df['Description'].str.contains('XBRL INSTANCE DOCUMENT', regex=False) | df['Type'].str.contains(r'EX-10[01]\.INS', regex=True)
        if extracted_condition.any():
            xrbl = os.path.basename(df[extracted_condition].squeeze()['Document'])
            self._download_urls_to_temp(df['Document'].to_numpy().tolist())
            xrbl = self._load_xrbl(os.path.join(self.save_directory, self.temp_dir, xrbl))
            if raw_xrbl:
                return xrbl
            self._clear_temp()
            return self._xrbl_to_df(xrbl, strip_styling)
        else:
            print(print_url)
            raise ValueError('Not implemented')
            
    def _extract_from_xrbl_dimension(self, xrbl_fact, dimension:int, property_axis:int):
        vals = list(xrbl_fact.context.qnameDims.values())
        if len(vals) <= dimension:
            return None
        vals = vals[dimension].propertyView
        if len(vals) <= property_axis:
            return None
        return vals[property_axis]
    
    def _extract_period_date_from_html_header(self, soup):
        period_match = False
        header = soup.select('#formDiv .formContent')
        if len(header) == 0:
            raise RuntimeError('Header does not exist')
        else:
            header = header[0]
        for div in header.select('.infoHead, .info'):
            if period_match == True:
                period = div.text
                return dt.datetime.strptime(period, '%Y-%m-%d')
            elif div.text == 'Period of Report':
                period_match = True
            
    def _in_memory_xrbl_from_html_url(self, url:str):
        if url[0:5] == 'edgar':
            url = self._join_partial_url(url)
        data_table = self._download_data_table(url)
        if data_table:
            return self._extract_xrbl_facts_from_datatable(data_table[0], print_url=url, raw_xrbl=True)
        else:
            raise RuntimeError('URL does not contain XRBL data')
            
            
class Edgar_10KQ_Scraper(Edgar_XRBL_Scraper):
    def __init__(self, time_sleep:float=.1, save_directory:str='edgar',
                 edgar_index_from_year:int=1993, skip_server_errors:bool=False):
        super().__init__(time_sleep, save_directory, edgar_index_from_year)
        self._reduce_edgar_index()
        self._strip_regex_dict = {'attachment':ATTACHMENT_REMOVAL_REGEX1, 'attachment2':ATTACHMENT_REMOVAL_REGEX2}
        self._entity_regex_dict = HTML_ENTITY_DICT
        self._entity_regex = HTML_ENTITY_REGEX
        self._bad_unicode_regex = UNICODE_REMOVAL_REGEX
        
        # The functionality of this is limited to the initial download of the html table to simplify the API.
        # Functions need to be updated to bypass additional unpredictable behavior on the Edgar urls.
        self.skip_server_errors = skip_server_errors
        
    def _reduce_edgar_index(self):
        print('Reducing index to 10Ks and 10Qs.')
        self.edgar_index = self.edgar_index[self.edgar_index['form'].str.contains('^10-?[KQ]', regex=True)].copy()
    
    def _strip_regex(self, text:str):
        for operation in self._strip_regex_dict.values():
            text = operation.sub('', text)
        return text
    
    def _strip_message_header_and_footer(self, text:str):
        if '-BEGIN PRIVACY-ENHANCED MESSAGE-' in text:
            start = text.find('==\n\n')
            if start >= 10 and start < 500:
                if '-END PRIVACY-ENHANCED MESSAGE-' in text:
                    end = text.rfind('\n--')
                    if end != -1:
                        return text[start+4:end]
                    else:
                        print('end not found')
                return text[start+4:]
            else:
                print(f'Header at strange size of {start}')
        return text
    
    
    def _decode_and_strip_response_contents(self, contents:bytes, encoding:str):
        r = contents.decode(encoding)
        r = self._strip_message_header_and_footer(r)
        return self._strip_regex(r)
    
    def _execute_regex_operations(self, r:str):
        #regex = re.compile(r'(&\S{2,8}?;)')
        #print(list(set(regex.findall(u))))
        r = regex_keyword_dict_replacement(r, self._entity_regex, self._entity_regex_dict, ignore_case=True)
        r = replace_entities(r, keep = ['&lt;', '&gt;', '&amp;'], remove_illegal=False)
        r = self._bad_unicode_regex.sub('', r)
        r = looped_string_replacement(r, SPACE_CHAR_DICT)
        return remove_tags(r)
    
    def _find_undownloaded_files_from_edgar_df(self, column:str, directory:str, extension:str, trim_extension:bool=False):
        if trim_extension:
            df = pd.DataFrame(self.edgar_index[column].map(lambda x: os.path.splitext(os.path.basename(x))[0]))
        else:
            df = pd.DataFrame(self.edgar_index[column].map(lambda x: os.path.basename(x)))
        downloaded = pd.DataFrame(index=set(self._get_csv_list_filenames(directory, extension, trim_extension)))
        downloaded['exists']=True
        return df.join(downloaded, on=column, how='left')['exists'].isna()
    
    def _find_undownloaded_files_from_skip_log(self, column:str):
        df = pd.DataFrame(self.edgar_index[column].map(lambda x: os.path.basename(x)))
        skipped = os.path.join(self.save_directory, self.skip_file)
        skipped = pd.DataFrame(index=set([file.strip() for file in open_file_lines_general(skipped)]))
        skipped['exists']=True
        return df.join(skipped, on=column, how='left')['exists'].isna()
    
    def _exclude_previous_downloads(self):
        is_not_downloaded_txt = self._find_undownloaded_files_from_edgar_df('text_url', os.path.join(self.save_directory, self.text_dir), 'txt')
        is_not_downloaded_xrbl = self._find_undownloaded_files_from_edgar_df('html_url', os.path.join(self.save_directory, self.xrbl_dir), 'tsv', trim_extension=True)
        is_not_skipped = self._find_undownloaded_files_from_skip_log('html_url')
        is_not_downloaded = is_not_downloaded_txt & is_not_downloaded_xrbl & is_not_skipped
        print(f'Excluding {is_not_downloaded.size-is_not_downloaded.sum()} already downloaded files')
        return self.edgar_index[is_not_downloaded]
        
    def _save_text_url(self, url:str, dir_path:str, save_raw:bool=False):
        if save_raw:
            self._download_raw_From_url_to_disk(url, dir_path)
        r = self._get_from_url(url, soup_parser=None)
        fn = os.path.join(dir_path, os.path.basename(url))
        try:
            u = self._decode_and_strip_response_contents(r, 'utf-8')
            u = self._execute_regex_operations(u)
            u = u.encode('utf-8')
            save_file_general(u, fn, binary=True)
        except (UnicodeEncodeError, UnicodeDecodeError) as e:
            try:
                u = self._decode_and_strip_response_contents(r, 'windows-1252')
            except (UnicodeEncodeError, UnicodeDecodeError) as e:
                u = self._decode_and_strip_response_contents(r, 'ISO-8859-1')
            u = self._execute_regex_operations(u)
            u = u.encode('utf-8')
            save_file_general(u, fn, binary=True)
    
    def _scrape_single_form_to_disk(self, row, skip_xrbl:bool=False, skip_text:bool=False, save_raw:bool=False, cik:str='NOCIK',
                                   period_range:tuple=()):
        url = self._join_partial_url(row['html_url'])
        if self.skip_server_errors:
            html_page, response_code = self._get_from_url_override_codes(url, allow_codes=[503])
            if response_code != 200:
                self._sleep()
                return None
        else:
            html_page = self._get_from_url(url)
            
        if len(period_range) == 2:
            period = self._extract_period_date_from_html_header(html_page)
            if period < period_range[0] or period > period_range[1]:
                line = os.path.basename(url) + '\n'
                save_file_general(line, os.path.join(self.save_directory, self.skip_file), append=True)
                self._sleep()
                return None
        elif len(period_range) == 0:
            pass
        else:
            raise ValueError('Period must be a tuple of start date and end date')
            
        data_table = self._select_data_table(html_page)
        if data_table and not skip_xrbl:
            directory = os.path.join(self.save_directory, self.xrbl_dir, cik)
            fn = os.path.join(directory, os.path.splitext(os.path.basename(url))[0]+'.tsv')
            df = self._extract_xrbl_facts_from_datatable(data_table[0], print_url=url)
            df.to_csv(fn, sep='\t', index=False)
        elif not data_table and not skip_text:
            directory = os.path.join(self.save_directory, self.text_dir, cik)
            url = self._join_partial_url(row['text_url'])
            self._save_text_url(url, directory, save_raw)
            self._sleep()
        else:
            print('Skipping ', row['text_url'])
    
    def _get_from_cik(self, cik:int, save_raw:bool=False, df=None, period_range:tuple=()):
        if df is None:
            df = self.edgar_index
        for folder in [self.xrbl_dir, self.text_dir]:
            os.makedirs(os.path.join(self.save_directory, folder, str(cik)), exist_ok=True)
        forms = df[df['cik'] == cik]
        for form in forms.iterrows():
            row = form[1]
            self._scrape_single_form_to_disk(row, save_raw, cik=str(cik), period_range=period_range)
    
    def scrape_from_cik_list(self, cik_list:list, save_raw:bool=False, skip_downloaded:bool=True):
        if skip_downloaded:
            df = self._exclude_previous_downloads()
        else:
            df = None
        if len(cik_list) == 0:
            print('No CIKs provided. Terminating function')
            return
        if isinstance(cik_list[0], int):
            for cik in cik_list:
                self._get_from_cik(cik, save_raw, df)
        elif len(cik_list[0]) == 2:
            for cik, period_range in cik_list:
                self._get_from_cik(cik, save_raw, df, period_range)
    
    
class Edgar_10KQ_Text_Sample_Scraper(Edgar_10KQ_Scraper):
        
    def _create_sample(self, split_column:str='', date_column:str='', sample_per_split:int=100):
        df = self.edgar_index
        dfs = []
        for value in df[split_column].unique():
            column_condition = df[split_column] == value
            for year in range(1993, dt.datetime.now().year):
                condition = (df[date_column] >= str(year)) & (df[date_column] < str(year + 1)) & column_condition
                subset = df[condition]
                if subset.shape[0] < sample_per_split:
                    dfs.append(subset)
                else:
                    dfs.append(subset.sample(sample_per_split))
        return pd.concat(dfs)
    
    def get_text_sample(self, df=pd.DataFrame, start=0, save_raw:bool=False, sample_per_split:int=2000):
        if df.empty:
            df = self._create_sample('form', 'date', sample_per_split)
            df.to_csv(os.path.join(self.save_directory, 'sample.csv'), index=False)
        for form in df.iloc[start:].iterrows():
            row = form[1]
            # Excludes documents in XRBL format
            url = self._join_partial_url(row['html_url'])
            datatable = self._download_data_table(url)
            if datatable:
                continue
            url = self._join_partial_url(row['text_url'])
            self._save_text_url(url, os.path.join(self.save_directory, self.text_dir), save_raw)
            self._sleep()
            
        
class Edgar_10KQ_Outstanding_Share_Scraper(Edgar_10KQ_Scraper):
    def __init__(self, time_sleep:float=.1, save_directory:str='edgar',
                 edgar_index_from_year:int=1993, skip_server_errors:bool=False):
        super().__init__(time_sleep, save_directory, edgar_index_from_year, skip_server_errors)
        self._cik_col = 'cik'
        self._class_col = 'class'
        self._date_col = 'endDatetime'
        self._fact_col = 'fact'
        self._filename_col = 'filename'
        self._outstanding_fact = 'dei:EntityCommonStockSharesOutstanding'
        self._outstanding_column = 'shares_outstanding'
        self._period_col = 'period'
        self._dim_col = 'dim01'
        self._value_col = 'value'
        #self._class_xrbl_regex = re.compile('class([a-z]\d?)', flags=re.I)
        self._class_xrbl_regex = re.compile('(?:class.*)?class([a-z]\d?)', flags=re.I)
        
    def _get_local_outlier_factor_inliers(self, df, k:int, p:int=2, dampen:int=1):
        if k > df.shape[0]:
            return pd.Series(True, index=df.index)
        model = LocalOutlierFactor(n_neighbors=k, p=p)
        scaler = StandardScaler()
        inputs = scaler.fit_transform(pd.DataFrame([df[self._date_col].view('int64'), df[self._value_col]]).T)
        # The columns are scaled, but this will increase the size of the temporal values to make the system more forgiving
        # to detected outliers.
        inputs[:,0] = inputs[:,0] * dampen
        return np.where(model.fit_predict(inputs) == 1, True, False)
    
    def _get_sorted_list_sharecount_csv_files(self, file_search_term:str='*shares_auto.csv'):
        search = os.path.join(self.save_directory, self.output_dir, file_search_term)
        return sorted(glob.glob(search), key=lambda x: int(os.path.basename(x).split('_', 1)[0]))
        
    def _outstanding_dates_quarterly_resample(self, df, share_class:str=''):
        if df.empty:
            return df
        if share_class:
            df = df[df[self._class_col]==share_class]
        df = df[self._get_local_outlier_factor_inliers(df, k=5, p=2, dampen=6)].drop_duplicates(subset=[self._date_col]).copy()
        df = resample_to_quarter_ends(df, self._date_col, self._value_col)
        if share_class:
            df[self._class_col] = share_class
        return df
    
    def _outstanding_share_timelines_from_cik_folder(self, cik:int):
        dfs = []
        cik_dir = os.path.join(self.save_directory, self.xrbl_dir, str(cik))
        files = os.listdir(cik_dir)
        if len(files) == 0:
            return pd.DataFrame()
        for file in files:
            df = pd.read_csv(os.path.join(cik_dir, file), sep='\t', lineterminator='\n',
                             dtype={self._value_col:str, self._dim_col:str, self._date_col:str})
            df[self._date_col] = pd.to_datetime(df[self._date_col], errors='coerce')
            dfs.append(self._xrbl_extract_shares_outstanding_and_dates_from_df(df))
        df = pd.concat(dfs, ignore_index=True)
        
        dfs = []
        df[self._value_col] = df[self._value_col].astype(float).round().astype(int)
        class_isna = df[self._class_col].isna()

        dfs.append(self._outstanding_dates_quarterly_resample(df[class_isna]))
        for share_class in df[~class_isna][self._class_col].unique():
            dfs.append(self._outstanding_dates_quarterly_resample(df, share_class))

        df = pd.concat(dfs, ignore_index=True).rename(columns={self._date_col:self._period_col, self._value_col:self._outstanding_column})
        df.insert(1, self._cik_col, cik)
        return df
    
    def _xrbl_extract_shares_outstanding_and_dates_from_df(self, df):

        df = df[df[self._fact_col]==self._outstanding_fact].copy()
        df = df.dropna(subset=[self._value_col, self._date_col])
        df[self._class_col] = df[self._dim_col].str.extract(self._class_xrbl_regex, expand=False).str.lower()
        df[self._class_col] = np.where(df[self._dim_col].str.strip()=='us-gaap:NonvotingCommonStockMember', 'nonvoting', df[self._class_col])
        is_general = df[self._dim_col].isna() | (df[self._dim_col] == 'us-gaap:CommonStockMember')
        df = df[~df[self._class_col].isna() | is_general]
        df = df[[self._date_col, self._class_col, self._value_col]]
        return df.drop_duplicates(subset=[self._class_col, self._date_col], keep='first').sort_values(by=[self._class_col, self._date_col], na_position='first')
    
    def _xrbl_folder_import_full_dfs(self, max_len_value_col:int=300):

        dfs = []        
        for file in self._get_csv_list(os.path.join(self.save_directory, self.xrbl_dir), extension='tsv'):
            _, cik, fn = file.rsplit('/', 2)
            df = pd.read_csv(file, sep='\t', lineterminator='\n', dtype={'value':str})
            if max_len_value_col:
                df = df[df[self._value_col].str.len() < max_len_value_col]
            df.insert(0, self._cik_col, cik)
            df.insert(1, self._filename_col, fn)
            dfs.append(df)
        return pd.concat(dfs, ignore_index=True)
    
    def _save_cik_history_outstanding_to_disk(self, df, cik:str):
        auto_path, manual_path = [os.path.join(self.save_directory, self.output_dir, f'{cik}_shares_{kind}.csv') for kind in ['auto', 'manual']]
        if not os.path.exists(manual_path):
            pd.DataFrame(columns=df.columns).to_csv(manual_path, index=False)
        df.to_csv(auto_path, index=False)
    
    def cik_history_outstanding_shares_from_downloads(self, export_csvs:bool=False):
        dfs = []
        for cik in os.listdir(os.path.join(self.save_directory, self.xrbl_dir)):
            df = self._outstanding_share_timelines_from_cik_folder(cik)
            if df.columns.empty:
                print(f'No data was scraped for cik #{cik}')
                continue
            elif df.empty:
                print(f'Missing values for cik #{cik}')
                continue
            if export_csvs:
                self._save_cik_history_outstanding_to_disk(df, cik)
            dfs.append(df)
        return pd.concat(dfs)
    
    def get_sharecount_df_from_disk(self, i:int=None, cik:int=None, merged=False):
        
        if i is not None and cik is not None:
            raise ValueError('Must choose between an list index (traversing sorted files in the directory) or a cik #.')
        elif i is not None:
            full_path = self._get_sorted_list_sharecount_csv_files()[i]
        elif cik is not None:
            full_path = os.path.join(self.save_directory, self.output_dir, f'{cik}_shares_auto.csv')
        else:
            raise ValueError('Must choose between an list index (traversing sorted files in the directory) or a cik #.')
        
        if os.path.exists(full_path):
            csv = [pd.read_csv(full_path)]
        else:
            csv = [None]
        path, fn = os.path.split(full_path)
        path = os.path.join(path, fn.rsplit('_', 1)[0] + '_manual.csv')
        if os.path.exists(path) and merged:
            manual = pd.read_csv(path)
            if not manual.empty:
                csv.append(manual)
        if csv[0] is None:
            message = path + ' manual only' if len(csv) > 1 else full_path + ' does not exist'
            print(message)
            return csv[1] if len(csv) > 1 else None
        else:
            message = full_path + ' and manual' if len(csv) > 1 else full_path
            print(message)
            csv = pd.concat(csv).drop_duplicates(subset=['period', 'cik', 'class'], keep='last').sort_values(by=['class', 'period'])
            return csv